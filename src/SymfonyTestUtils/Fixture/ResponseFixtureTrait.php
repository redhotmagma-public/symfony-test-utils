<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

trait ResponseFixtureTrait
{

    use TestPathTrait;

    private static $extension = 'expected.json';

    public function storeResponseFixture(Response $response, $skip = true): string
    {
        $testHttpResponse = TestHttpResponse::fromResponse($response);

        $fixtureDir = $this->fixtureDirectory();
        $fixtureFile = $this->fixtureFile(null, self::$extension);
        $fullPath = $fixtureDir . DIRECTORY_SEPARATOR . $fixtureFile;

        $fs = new Filesystem();
        if (!$fs->exists($fixtureDir)) {
            $fs->mkdir($fixtureDir);
        }

        $fs->dumpFile($fullPath, json_encode($testHttpResponse, JSON_PRETTY_PRINT));

        if ($skip) {
            TestCase::markTestSkipped('Response fixture created. Remove this call and test again.');
        }

        return file_get_contents($fullPath);
    }

    /**
     * Moves a fixture to its new place. This is usually done when needed to rename test class / test method.
     *
     * @param string $testClass
     * @param string $testMethod
     */
    public function moveResponseFixture(string $testClass, string $testMethod): void
    {
        $originalDir = $this->fixtureDirectory($testClass);
        $originalFileName = $this->fixtureFile($testMethod, self::$extension);
        $originalFile = $originalDir . DIRECTORY_SEPARATOR . $originalFileName;

        $newDir = $this->fixtureDirectory();
        $newFileName = $this->fixtureFile(null, self::$extension);
        $newFile = $newDir . DIRECTORY_SEPARATOR . $newFileName;

        $fs = new Filesystem();
        $fs->mkdir($newDir);
        $fs->rename($originalFile, $newFile);
    }

    /**
     * Loads Response fixture.
     *
     * @return TestHttpResponse
     */
    public function loadResponseFixture(): TestHttpResponse
    {
        $fixtureDir = $this->fixtureDirectory();
        $fixtureFile = $this->fixtureFile(null, self::$extension);
        $fullPath = $fixtureDir . DIRECTORY_SEPARATOR . $fixtureFile;

        $fs = new Filesystem();
        if (!$fs->exists($fullPath)) {
            TestCase::fail('Response fixture does not exist.');
        }

        $testHttpResponse = TestHttpResponse::fromFileContent(file_get_contents($fullPath));

        return $testHttpResponse;
    }
}
