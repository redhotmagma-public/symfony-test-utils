<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class TestHttpResponse
{

    const IGNORED_HEADERS = [
        'cache-control',
        'date',
        'x-debug-token'
    ];

    public $statusCode;

    public $headers;

    public $body;

    private function __construct(array $content)
    {
        foreach ($content as $property => $value) {
            $this->$property = $value;
        }
    }

    /**
     * @param Response $response
     *
     * @return TestHttpResponse
     */
    public static function fromResponse(Response $response)
    {
        $content = [
            'statusCode' => $response->getStatusCode(),
            'headers' => self::filterHeaders($response->headers),
            'body' => json_decode($response->getContent(), true)
        ];

        return new self($content);
    }

    /**
     * @param string $fileContent
     *
     * @return TestHttpResponse
     */
    public static function fromFileContent(string $fileContent)
    {
        $content = json_decode($fileContent, true);

        return new self($content);
    }

    /**
     * @param ResponseHeaderBag $headers
     *
     * @return array
     */
    private static function filterHeaders(ResponseHeaderBag $headers): array
    {
        $filteredHeaders = array_filter($headers->all(), function (string $headerName) {
            return !in_array($headerName, self::IGNORED_HEADERS);
        }, ARRAY_FILTER_USE_KEY);

        return $filteredHeaders;
    }
}
