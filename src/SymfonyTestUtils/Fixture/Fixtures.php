<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contains factory methods to ease fixture assetions.
 */
class Fixtures
{

    /**
     * Asserts that a client's Response equals to an expected one.
     *
     * @param TestHttpResponse          $expected
     * @param TestHttpResponse|Response $actual
     * @param string                    $message
     */
    public static function assertResponseEquals(TestHttpResponse $expected, $actual, string $message = '')
    {
        $actual = $actual instanceof Response ? TestHttpResponse::fromResponse($actual) : $actual;

        TestCase::assertEquals($expected, $actual, $message);
    }
}
