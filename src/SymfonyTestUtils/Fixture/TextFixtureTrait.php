<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

trait TextFixtureTrait
{

    use TestPathTrait;

    private static $extension = 'expected.txt';

    /**
     * @param string $content
     */
    public function createTextFixture($content = ''): void
    {
        $fixtureDir = $this->fixtureDirectory();
        $fixtureFile = $this->fixtureFile(null, self::$extension);
        $fullPath = $fixtureDir . DIRECTORY_SEPARATOR . $fixtureFile;

        $fs = new Filesystem();
        if (!$fs->exists($fixtureDir)) {
            $fs->mkdir($fixtureDir);
        }

        $fs->dumpFile($fullPath, $content);

        TestCase::markTestSkipped('Response fixture created. Remove this call and test again.');
    }

    /**
     * Loads a text fixture, or create it when it does not exist. In the later, the test will be skipped.
     *
     * When a replacement array is provided, all occurrences of the array keys in the original fixture will be replaced by
     * the provided array values. Example of usage:
     *
     * <code>
     * $this->loadTextFixture(['replace-me' => 'This is the replacement'])
     * </code>
     *
     * @param array $replacements
     *
     * @return string
     */
    public function loadTextFixture(array $replacements = []): string
    {
        $fixtureDir = $this->fixtureDirectory();
        $fixtureFile = $this->fixtureFile(null, self::$extension);
        $fullPath = $fixtureDir . DIRECTORY_SEPARATOR . $fixtureFile;

        $fs = new Filesystem();
        if (!$fs->exists($fullPath)) {
            $this->createTextFixture();
        }

        $textContent = file_get_contents($fullPath);
        $textContent = str_replace(array_keys($replacements), array_values($replacements), $textContent);

        return trim($textContent);
    }
}
