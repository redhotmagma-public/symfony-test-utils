<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @method static self json(TestCase $testCase)
 * @method static self xml(TestCase $testCase)
 * @method static self yaml(TestCase $testCase)
 * @method static self sql(TestCase $testCase)
 */
class FixturesIO
{
    /**
     * @var TestCase
     */
    private $testCase;

    /**
     * @var string
     */
    private $extension;

    public function __construct(TestCase $testCase, string $extension)
    {
        $this->testCase = $testCase;
        $this->extension = strtolower($extension);
    }

    public static function __callStatic($name, $arguments): self
    {
        return new self(array_shift($arguments), $name);
    }

    public function read(string $fileName = null): string
    {
        $fixtureFile = $this->fullFileName($fileName);

        if (!$this->fileExists($fileName)) {
            $this->write('', $fileName);
            $msg = sprintf('Fixture file does not exist. New file "%s" is created and test is skipped.', $fixtureFile);
            $this->testCase->markTestSkipped($msg);
        }

        return file_get_contents($fixtureFile);
    }

    public function path(string $fileName = null): string
    {
        return $this->fullFileName($fileName);
    }

    public function write(string $content, string $fileName = null)
    {
        (new Filesystem())->mkdir($this->fixtureDirectory());
        $file = $this->fullFileName($fileName);

        file_put_contents($file, $content);
    }

    private function fileExists(string $fileName = null): bool
    {
        $fixtureFile = $this->fullFileName($fileName);

        return (new Filesystem())->exists($fixtureFile);
    }

    private function fixtureDirectory(): string
    {
        $reflector = new \ReflectionObject($this->testCase);
        $fn = $reflector->getFileName();
        $className = $reflector->getShortName();

        return dirname($fn) . '/_fixtures/' . $className;
    }

    private function fullFileName(?string $fileName): string
    {
        $dir = $this->fixtureDirectory();
        $methodName = $this->testCase->getName(false);
        $fileName = ($fileName ?? $methodName) . '.' . $this->extension;

        return $dir . '/' . $fileName;
    }
}
