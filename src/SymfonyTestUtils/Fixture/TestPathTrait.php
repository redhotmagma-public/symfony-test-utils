<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\AssertionFailedError;

/**
 * @internal
 */
trait TestPathTrait
{

    /**
     * Extracts fixture file name.
     *
     * @param string|null $fileName
     * @param string      $extension
     *
     * @return string
     */
    protected function fixtureFile(string $fileName = null, string $extension = 'json'): string
    {
        $fileName = $fileName ?? $this->getName(false);
        $fileName .= '.' . $extension;

        return $fileName;
    }

    /**
     * Extract fixtures directory for given test class.
     *
     * @param string|null $class
     *
     * @return string
     */
    protected function fixtureDirectory(?string $class = null): string
    {
        try {
            $class = $class ?? static::class;
            $reflector = new \ReflectionClass($class);
            $fileName = $reflector->getFileName();
            $className = $reflector->getShortName();

            return dirname($fileName) . DIRECTORY_SEPARATOR . '_fixtures' . DIRECTORY_SEPARATOR . $className;
        } catch (\ReflectionException $e) {
            throw new AssertionFailedError('Test precondition failed: cannot read class "' . $class . '"');
        }
    }
}
