<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Client;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Creates a Symfony Client using Builder desing pattern. It may be extended with project-specific rules.
 */
class Builder
{

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var array
     */
    private $files = [];

    /**
     * @var array
     */
    private $server = [];

    /**
     * @var mixed
     */
    private $content;

    /**
     * @param KernelInterface $kernel
     * @param string          $uri
     */
    public function __construct(KernelInterface $kernel, string $uri)
    {
        $this->uri = $uri;
        $this->kernel = $kernel;
    }

    /**
     * Sets path and query parameters.
     *
     * @param array $parameters
     *
     * @return self
     */
    public function withParameters(array $parameters): self
    {
        foreach ($parameters as $key => $value) {
            $uriParamCount = 0;
            $param = $value;
            if (!is_scalar($parameters[$key])) {
                $param = json_encode($value);
            }

            $this->uri = str_replace('{' . $key . '}', $param, $this->uri, $uriParamCount);
            if ($uriParamCount > 0) {
                // remove replaced values
                unset($parameters[$key]);
            }
        }

        $this->parameters = $parameters;

        return $this;
    }

    /**
     * The Files to be uploaded.
     *
     * @param array $files
     *
     * @return self
     */
    public function uploadFiles(array $files): self
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Informs headers that need to be sent.
     *
     * @param array $headers
     *
     * @return self
     */
    public function sendHeaders(array $headers): self
    {
        $this->server = array_merge($this->server, $headers);

        return $this;
    }

    /**
     * Sets request's body content.
     *
     * @param $content
     *
     * @return self
     */
    public function withBody($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Perform GET method.
     *
     * @return Client
     */
    public function get(): Client
    {
        return $this->createClient(Request::METHOD_GET);
    }

    /**
     * Perform POST method.
     *
     * @param mixed $body
     *
     * @return Client
     */
    public function post($body = null): Client
    {
        $this->content = $body ?? $this->content;
        return $this->createClient(Request::METHOD_POST);
    }

    /**
     * Perform PUT method.
     *
     * @param mixed $body
     *
     * @return Client
     */
    public function put($body = null): Client
    {
        $this->content = $body ?? $this->content;
        return $this->createClient(Request::METHOD_PUT);
    }

    /**
     * Perform DELETE method.
     *
     * @return Client
     */
    public function delete(): Client
    {
        return $this->createClient(Request::METHOD_DELETE);
    }

    /**
     * Creates the client, based on current builder properties.
     *
     * @param string $method
     *
     * @return Client
     */
    private function createClient(string $method): Client
    {
        $this->kernel->boot();
        $container = $this->kernel->getContainer();

        $client = $this->modifyClient($this->getClient($container));
        $client->setServerParameters($this->server);

        $uri = $this->uri . (!empty($this->parameters) ? '?' . http_build_query($this->parameters) : '');
        $client->request($method, $uri, $this->parameters, $this->files, $this->server, $this->content);

        return $client;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return Client
     */
    private function getClient(ContainerInterface $container): Client
    {
        return $container->get('test.client');
    }

    /**
     * Applies project-specific modifications to the client.
     *
     * @param Client $client
     *
     * @return Client
     */
    protected function modifyClient(Client $client): Client
    {
        return $client;
    }

    /**
     * @return string
     */
    protected function uri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $key
     *
     * @return Builder
     */
    protected function removeHeader(string $key): self
    {
        unset($this->server[$key]);

        return $this;
    }
}
