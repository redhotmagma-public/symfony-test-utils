<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Constraint;

use PHPUnit\Framework\Constraint\Constraint;
use PHPUnit\Framework\Constraint\IsEqual;
use PHPUnit\Framework\Constraint\IsInstanceOf;
use PHPUnit\Util\InvalidArgumentHelper;
use redhotmagma\SymfonyTestUtils\Constraint\Helper\ToString;

class IsModelEqual extends Constraint
{

    private const INVALID_INSTANCE = 'invalid_instance';

    private const NOT_EQUALS = 'not_equals';

    private const PROPERTY_VALUE = 'property_value';

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var array
     */
    private $properties;

    /**
     * @var array
     */
    private $unmatchedProperties = [];

    /**
     * @var string
     */
    private $error;

    /**
     * @var IsEqual
     */
    private $isEquals;

    /**
     * @var IsInstanceOf
     */
    private $isInstanceOf;

    /**
     * @param       $value
     * @param array $properties
     */
    public function __construct($value, array $properties = [])
    {
        if (!is_object($value)) {
            throw InvalidArgumentHelper::factory(1, 'object');
        }

        $this->value = $value;
        $this->properties = $properties;

        $this->isEquals = new IsEqual($this->value);
        $this->isInstanceOf = new IsInstanceOf(get_class($this->value));
    }

    /**
     * @inheritdoc
     */
    protected function matches($other): bool
    {
        if ($this->value === $other) {
            return true;
        }

        if (!$this->isInstanceOf->evaluate($other, '', true)) {
            $this->error = self::INVALID_INSTANCE;
            return false;
        }

        if (empty($this->properties)) {
            $this->error = self::NOT_EQUALS;
            return $this->isEquals->matches($other);
        }

        $expectedRO = new \ReflectionObject($this->value);
        $otherRO = new \ReflectionObject($other);

        foreach ($this->properties as $property) {
            $expectedValue = $this->getValue($expectedRO, $this->value, $property);
            $otherValue = $this->getValue($otherRO, $other, $property);

            /** @noinspection TypeUnsafeComparisonInspection */
            if ($expectedValue != $otherValue) {
                $this->unmatchedProperties[$property] = [$expectedValue, $otherValue];
                $this->error = self::PROPERTY_VALUE;
            }
        }

        return empty($this->error);
    }

    /**
     * @inheritdoc
     */
    public function toString(): string
    {
        return 'opa!';
    }

    /**
     * @inheritdoc
     */
    protected function failureDescription($other): string
    {
        switch ($this->error) {
            case self::INVALID_INSTANCE:
                $description = $this->isInstanceOf->failureDescription($other);
                break;
            case self::NOT_EQUALS:
                $description = $this->isEquals->failureDescription($other);
                break;
            default:
                $description = 'properties from ' . $this->exporter->shortenedExport($other);
                $description .= ' matches expected properties from ' . $this->exporter->shortenedExport($this->value) . '.';
                $description .= "Invalid values: \n";

                $bits = [];
                $toString = new ToString(4, 4);
                foreach ($this->unmatchedProperties as $name => $values) {
                    $property = $name;
                    $expected = $toString->convert($values[0]);
                    $got = $toString->convert($values[1]);

                    $description .= "$property:\n  - $expected\n  + $got\n";
                }

                $description .= implode("\n", $bits);

                break;
        }

        return $description;
    }

    /**
     * @param \ReflectionObject $reflector
     * @param mixed             $object
     * @param string            $property
     *
     * @return mixed
     */
    private function getValue(\ReflectionObject $reflector, $object, string $property)
    {
        if ($reflector->hasProperty($property)) {
            // Tries to get property directly
            $property = $reflector->getProperty($property);
            $property->setAccessible(true);
            $value = $property->getValue($object);
        } elseif ($reflector->hasMethod($methodName = 'get' . ucfirst($property))) {
            // Tries to get property by getter
            $method = $reflector->getMethod($methodName);
            $value = $method->invoke($object);
        } else {
            $message = sprintf('Could not get value for property %s from class %s.', $property, get_class($object));
            throw new \InvalidArgumentException($message);
        }

        return $value;
    }
}
