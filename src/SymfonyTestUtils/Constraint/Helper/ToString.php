<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Constraint\Helper;

class ToString
{

    /**
     * @var int
     */
    private $ident;

    /**
     * @var int
     */
    private $offset;

    public function __construct(int $ident = 4, int $offset = 0)
    {
        $this->ident = $ident;
        $this->offset = $offset;
    }

    /**
     * Converts a scalar, null or array to its string representation.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function convert($value): string
    {
        return $this->convertValue($value, $this->ident + $this->offset);
    }

    private function convertValue($value, int $ident)
    {
        if (is_scalar($value)) {
            $converted = (string) $value;
        } elseif (is_null($value)) {
            $converted = 'null';
        } elseif (is_array($value)) {
            $converted =  $this->convertArray($value, $ident);
        } else {
            $converted = '';
        }

        return $converted;
    }

    /**
     * @param array $value
     * @param int   $ident
     *
     * @return string
     */
    private function convertArray(array $value, int $ident)
    {
        $baseIdent = str_repeat(' ', $ident - $this->ident);
        $identation = str_repeat(' ', $ident);

        $converted[] = 'Array';
        $converted[] = $baseIdent . '(';

        $count = count($value);
        $current = 0;
        foreach ($value as $k => $v) {
            $line = $identation;
            $line .= is_string($k) ? "$k => " : '';
            $line .= $this->convertValue($v, $ident + $this->ident);
            $line .= $count > ++$current ? ',' : '';

            $converted[] = $line;
        }

        $converted[] = $baseIdent . ')';

        return implode("\n", $converted);
    }
}
