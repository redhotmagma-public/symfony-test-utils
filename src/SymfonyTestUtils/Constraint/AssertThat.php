<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Constraint;

use PHPUnit\Framework\TestCase;

/**
 * Contains factory methods to ease assertions using PHPUnit's Constraint implementations.
 */
class AssertThat
{

    /**
     * Asserts that a model (object) is of same class as an expected one, and their chosen properties are equal.
     * Those properties are accessed either directly (using reflection) or by calling their getter method. In the latter,
     * the method should be called getMyProperty and should not accept any parameters.
     *
     * When no properties are set, works as TestCase::assertEquals.
     *
     * @param mixed  $expected   An object
     * @param mixed  $actual     The object to be compared
     * @param array  $properties Properties from both objects to compare
     * @param string $message
     */
    public static function modelEquals($expected, $actual, array $properties = [], string $message = ''): void
    {
        $isModelEqual = new IsModelEqual($expected, $properties);

        TestCase::assertThat($actual, $isModelEqual, $message);
    }
}
