#!/usr/bin/env bash

docker run -d --rm --workdir=/app -u $UID -v `pwd`:/app --name symfony_test_utils -it webdevops/php-dev:8.3
