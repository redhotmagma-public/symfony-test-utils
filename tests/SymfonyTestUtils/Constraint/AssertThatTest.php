<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils\Constraint;

use PHPUnit\Framework\TestCase;
use redhotmagma\SymfonyTestUtils\Constraint\AssertThat;
use stdClass;

class AssertThatTest extends TestCase
{
    public function testShouldAssertThatModelsPropertiesAreEqual(): void
    {
        $expected = new stdClass();
        $expected->someValue = 'groot';
        $expected->otherValue = 'mine';
        $expected->someOtherValue = 'groot';

        $actual = clone $expected;
        $actual->someOtherValue = 'rocket';

        AssertThat::modelEquals($expected, $actual, ['someValue', 'otherValue']);
    }

    public function testShouldAssertThatSubModelIsEqual(): void
    {
        $expected = new stdClass();
        $expected->someValue = 'groot';
        $expected->someOtherValue = 'groot';
        $expected->subModule = $expected;

        $actual = clone $expected;
        $actual->someOtherValue = 'rocket';
        $actual->subModule = clone $expected;

        AssertThat::modelEquals($expected, $actual, ['subModule']);
    }

    public function testShouldAssertThatSameSubModelIsEqual(): void
    {
        $expected = new stdClass();
        $expected->someValue = 'groot';
        $expected->someOtherValue = 'groot';
        $expected->subModule = $expected;

        $actual = clone $expected;
        $actual->someOtherValue = 'rocket';

        AssertThat::modelEquals($expected, $actual, ['subModule']);
    }
}
