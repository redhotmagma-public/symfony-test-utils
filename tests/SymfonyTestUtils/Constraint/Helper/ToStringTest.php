<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils\Constraint\Helper;

use PHPUnit\Framework\TestCase;
use redhotmagma\SymfonyTestUtils\Constraint\Helper\ToString;
use redhotmagma\SymfonyTestUtils\Fixture\TextFixtureTrait;

/**
 * Tests for {@link ToString}
 */
class ToStringTest extends TestCase
{

    use TextFixtureTrait;

    /**
     * @var ToString
     */
    private $toString;

    protected function setUp():void
    {
        $this->toString = new ToString();
    }

    /**
     * @dataProvider scalarValues
     *
     * @param $toConvert
     */
    public function testShouldConverScalarValues($toConvert)
    {
        $expected = (string) $toConvert;

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function scalarValues()
    {
        return [
            ['abc'],
            [1],
            [1.99],
        ];
    }

    public function testShouldConvertNullValue()
    {
        $toConvert = null;
        $expected = 'null';

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertArray()
    {
        $toConvert = ['abc'];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertArrayWithSeveralValues()
    {
        $toConvert = ['abc', 5, 1.99];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertArrayWithKey()
    {
        $toConvert = ['abc' => 'def'];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertSubArray()
    {
        $toConvert = [
            'a' => ['def', 'k' => 'ghi', 'i' => 5, 'j' => null]
        ];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertArrayUsingOtherIdentationValue()
    {
        $this->toString = new ToString(6);
        $toConvert = [
            'a' => ['def', 'k' => 'ghi']
        ];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }

    public function testShouldConvertArrayUsingOffsetValue()
    {
        $this->toString = new ToString(6, 3);
        $toConvert = [
            'a' => ['def', 'k' => 'ghi']
        ];
        $expected = $this->loadTextFixture();

        $converted = $this->toString->convert($toConvert);

        $this->assertEquals($expected, $converted);
    }
}
