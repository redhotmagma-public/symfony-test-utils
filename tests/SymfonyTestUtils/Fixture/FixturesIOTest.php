<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils\Fixture;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\vfsStreamException;
use org\bovigo\vfs\vfsStreamFile;
use PHPUnit\Framework\SkippedTestError;
use PHPUnit\Framework\TestCase;
use redhotmagma\SymfonyTestUtils\Fixture\FixturesIO;
use redhotmagma\Tests\SymfonyTestUtils\DirMock;

class FixturesIOTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private static $testRootDir;

    public static function setUpBeforeClass():void
    {
        self::$testRootDir = vfsStream::setup();

        DirMock::register(FixturesIO::class);
        DirMock::withDir(self::$testRootDir->url());
    }

    public function setUp():void
    {
        // Need to clean up vfsStream after each test
        self::$testRootDir = vfsStream::setup();
    }

    public static function tearDownAfterClass():void
    {
        DirMock::disable();
    }

    /**
     * @dataProvider extensions
     *
     * @param string $type
     */
    public function testShouldLoadFixture(string $type)
    {
        $expected = 'some content to check';
        $this->addFile('_fixtures/FixturesIOTest/testShouldLoadFixture.' . strtolower($type), $expected);

        $actual = (new FixturesIO($this, $type))->read();

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider extensions
     *
     * @param string $type
     */
    public function testShouldLoadFixtureWithOverriddenFileName(string $type)
    {
        $expected = 'some content to check';
        $this->addFile('_fixtures/FixturesIOTest/testShouldLoadFixture.' . strtolower($type), $expected);

        $actual = (new FixturesIO($this, $type))->read('testShouldLoadFixture');

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider extensions
     *
     * @param string $type
     */
    public function testShouldShowFixturePath(string $type)
    {
        $expected = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldShowFixturePath.' . strtolower($type);
        $this->addFile('_fixtures/FixturesIOTest/testShouldShowFixturePath.' . strtolower($type), '');

        $actual = (new FixturesIO($this, $type))->path();

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider extensions
     *
     * @param string $type
     */
    public function testShouldShowFixturePathWithOverriddenFilename(string $type)
    {
        $expected = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/overridden.' . strtolower($type);
        $this->addFile('_fixtures/FixturesIOTest/overridden.' . strtolower($type), '');

        $actual = (new FixturesIO($this, $type))->path('overridden');

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider extensions
     *
     * @param string $type
     */
    public function testShouldCreateFixturesIOBasedOnStaticMethodCall(string $type)
    {
        $expected = new FixturesIO($this, $type);

        $actual = FixturesIO::$type($this);

        $this->assertEquals($expected, $actual);
    }

    public function extensions(): array
    {
        return [
            ['json'],
            ['xml'],
            ['whatevs'],
            ['WHATEVS'],
        ];
    }

    public function testShouldWriteToFixture()
    {
        $this->addVfsDirectory('_fixtures/FixturesIOTest', self::$testRootDir);
        $expectedFileName = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldWriteToFixture.json';
        $expected = json_encode(['this' => 'is a json']);

        (new FixturesIO($this, 'json'))->write($expected);

        $this->assertFileExists($expectedFileName);
        $actual = file_get_contents($expectedFileName);
        $this->assertSame($expected, $actual);
    }

    public function testShouldWriteToFixtureWithOverriddenFileName()
    {
        $this->addVfsDirectory('_fixtures/FixturesIOTest', self::$testRootDir);
        $unwantedFileName = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldWriteToFixtureWithOverriddenFileName.json';
        $fileName = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldWriteToFixture.json';
        $expected = json_encode(['this' => 'is a json']);

        (new FixturesIO($this, 'json'))->write($expected, 'testShouldWriteToFixture');

        $this->assertFileExists($fileName);
        $this->assertFileNotExists($unwantedFileName);

        $actual = file_get_contents($fileName);
        $this->assertSame($expected, $actual);
    }

    public function testShouldCreateDirectoryWhenWritingToFileName()
    {
        $expectedDirectory = self::$testRootDir->url() . '/_fixtures/FixturesIOTest';
        $expected = json_encode(['this' => 'is a json']);

        (new FixturesIO($this, 'json'))->write($expected);

        $this->assertDirectoryExists($expectedDirectory);
    }

    /**
     * @dataProvider methodsForFixtureCreating
     *
     * @param string $extension
     */
    public function testShouldFailOnFixtureLoading(string $extension): void
    {
        $this->addVfsDirectory('_fixtures/FixturesIOTest', self::$testRootDir);
        $expected = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldFailOnFixtureLoading.' . $extension;

        try {
            (new FixturesIO($this, $extension))->read();
            $this->fail('The fixture file creation fails or this file already exists.');
        } catch (SkippedTestError $e) {
            $this->assertFileExists($expected);
        }
    }

    /**
     * @dataProvider methodsForFixtureCreating
     *
     * @param string $extension
     */
    public function testShouldFailWhenLoadingFixtureWithOverriddenFilename(string $extension)
    {
        $this->addVfsDirectory('_fixtures/FixturesIOTest', self::$testRootDir);
        $expected = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldFailOnFixtureLoading.' . $extension;
        $unwanted = self::$testRootDir->url() . '/_fixtures/FixturesIOTest/testShouldFailWhenLoadingFixtureWithOverriddenFilename.' . $extension;

        try {
            (new FixturesIO($this, $extension))->read('testShouldFailOnFixtureLoading');
            $this->fail('The fixture file creation fails or this file already exists.');
        } catch (SkippedTestError $e) {
            $this->assertFileNotExists($unwanted);
            $this->assertFileExists($expected);
        }
    }

    public function methodsForFixtureCreating(): array
    {
        return [
            ['json'],
            ['xml'],
            ['yaml'],
        ];
    }

    private function addFile(string $pathWithFileName, string $content)
    {
        $path = explode(DIRECTORY_SEPARATOR, $pathWithFileName);
        $fileName = array_pop($path);

        $dir = $this->addVfsDirectory(implode(DIRECTORY_SEPARATOR, $path), self::$testRootDir);
        $file = (new vfsStreamFile($fileName))->setContent($content);

        $dir->addChild($file);
    }

    private function addVfsDirectory(string $path, vfsStreamDirectory $rootDirectory): vfsStreamDirectory
    {
        $current = $rootDirectory;
        $folders = explode(DIRECTORY_SEPARATOR, $path);

        foreach ($folders as $folder) {
            try {
                $folder = new vfsStreamDirectory($folder);
            } catch (vfsStreamException $e) {
                $this->fail('Test precondition failed: cannot create directory: ' . $path);
            }
            $current->addChild($folder);

            $current = $folder;
        }

        return $current;
    }
}
