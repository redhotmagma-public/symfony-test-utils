<?php declare(strict_types=1);

namespace redhotmagma\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link TextFixtureTrait}
 */
class TextFixtureTraitTest extends TestCase
{

    use TextFixtureTrait;

    public function testShouldLoadTextFixture()
    {
        $expected = 'This is the text fixture';

        $actual = $this->loadTextFixture();

        TestCase::assertEquals($expected, $actual);
    }

    public function testShouldLoadTextFixtureWithReplacements()
    {
        $preCondition = 'This is the text {{ FIXTURE }}';
        $expected = 'This is the text fixture!';

        $original = $this->loadTextFixture();
        $actual = $this->loadTextFixture(['{{ FIXTURE }}' => 'fixture!']);

        TestCase::assertEquals($preCondition, $original, 'Test precondition failed: fixture should have a value to be replaced');
        TestCase::assertEquals($expected, $actual);
    }
}
