<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils\Fixture;

/**
 * Used in ResponseFixtureTestTraitTest class.
 */
class OtherClass
{
}
