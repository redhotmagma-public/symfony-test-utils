<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils\Fixture;

use PHPUnit\Framework\TestCase;
use redhotmagma\SymfonyTestUtils\Fixture\ResponseFixtureTrait;
use redhotmagma\SymfonyTestUtils\Fixture\TestHttpResponse;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests for {@link ResponseFixtureTrait}
 */
class ResponseFixtureTraitTest extends TestCase
{

    use ResponseFixtureTrait;

    protected function tearDown():void
    {
        $this->clearFixtureDirectory([$this->fixtureDirectory(), $this->fixtureDirectory(OtherClass::class)]);
    }

    public function testShouldStoreResponseFixture()
    {
        $file = $this->fixtureDirectory() . DIRECTORY_SEPARATOR . $this->fixtureFile(null, 'expected.json');
        TestCase::assertFileNotExists($file, 'Test precondition failed: Fixture file already exists!');

        $this->storeResponseFixture($this->createResponse(), false);

        TestCase::assertFileExists($file);
    }

    public function testShouldLoadResponseFixture()
    {
        $fileContent = $this->storeResponseFixture($this->createResponse(), false);
        $expected = TestHttpResponse::fromFileContent($fileContent);

        $actual = $this->loadResponseFixture();

        TestCase::assertEquals($expected, $actual);

    }

    public function testShouldMoveResponseFixture()
    {
        $fs = new Filesystem();
        $fs->mkdir($this->fixtureDirectory());
        $fs->dumpFile($this->fixtureDirectory() . DIRECTORY_SEPARATOR . 'oldMethodName.expected.json', 'some content');
        $file = $this->fixtureDirectory() . DIRECTORY_SEPARATOR . $this->fixtureFile(null, 'expected.json');

        $this->moveResponseFixture(self::class, 'oldMethodName');

        TestCase::assertFileExists($file);
    }

    public function testShouldMoveResponseFixtureFromOtherClass()
    {
        $fs = new Filesystem();
        $fs->mkdir($this->fixtureDirectory(OtherClass::class));
        $fs->dumpFile($this->fixtureDirectory(OtherClass::class) . DIRECTORY_SEPARATOR . 'fixtureFarAway.expected.json', 'some content');
        $file = $this->fixtureDirectory() . DIRECTORY_SEPARATOR . $this->fixtureFile(null, 'expected.json');

        $this->moveResponseFixture(OtherClass::class, 'fixtureFarAway');

        TestCase::assertFileExists($file);
    }

    public function testShouldThrowExceptionWhenMovingInexistentFile()
    {
        $this->expectException(IOException::class);

        $this->moveResponseFixture(self::class, 'thisIsNotTheFileYouAreLookingFor');
    }

    /**
     * @return Response
     */
    private function createResponse(): Response
    {
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->add(['Some-Header' => 'header value']);
        $response->setContent(json_encode(['property' => 'some body content here']));

        return $response;
    }

    /**
     * @param string[] $directories
     */
    private function clearFixtureDirectory(array $directories)
    {
        foreach ($directories as $directory) {
            $fs = new Filesystem();

            if ($fs->exists($directory)) {
                $scandir = scandir($directory);

                $scandir = array_filter($scandir, function (string $fileName) {
                    return !in_array($fileName, ['.', '..']);
                });
                $scandir[] = $directory;

                $fs->remove($scandir);
            }
        }
    }
}
