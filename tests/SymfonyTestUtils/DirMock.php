<?php declare(strict_types=1);

namespace redhotmagma\Tests\SymfonyTestUtils;

class DirMock
{
    private static $dir;
    private static $enabled = false;

    public static function withDir(string $dir): void
    {
        self::$enabled = true;
        self::$dir = $dir;
    }

    public static function disable(): void
    {
        self::$enabled = false;
    }

    public static function dir(string $path, int $levels = 1): string
    {
        if (self::$enabled) {
            return self::$dir;
        }

        return \dirname($path, $levels);
    }

    public static function register(string $class): void
    {
        $self = static::class;

        try {
            $mockedNs = (new \ReflectionClass($class))->getNamespaceName();
        } catch (\ReflectionException $e) {
            throw new \RuntimeException('Cannot mock \dirname function for class: ' . $class);
        }

        if (\function_exists($mockedNs . '\dirname')) {
            return;
        }

        eval(<<<EOPHP
namespace $mockedNs;

function dirname(string \$path, int \$levels = 1)
{
    return \\$self::dir(\$path, \$levels);
}
EOPHP
        );
    }
}
