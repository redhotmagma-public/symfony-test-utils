Test Bundle
===========

Bundle that consists of generic helper classes to aid unit and functional testing Symfony applications. Note that this bundle
should be self contained, meaning that it should not have dependencies to other internal projects.

## Topics

1. [Development](./docs/development/development.md)
1. [API Usage](./docs/usage/usage.md)

## Setup for development
1. Copy `.env.dist` to `.env`
1. Start a container `bin/start.sh` 
1. Get inside the container `bin/exec.sh bash` or run the commands from outside `bin/exec.sh vendor/bin/phpunit`
1. Stop the container `bin/stop.sh`

## Installation

Install it via composer.
```bash
composer require --dev redhotmagma/redhotmagma/symfony-test-utils dev-master
```
