Fixtures Content
================

# [Fixtures](../../src/SymfonyTestUtils/Fixture/Fixtures.php)

Contains factory methods to assert tests based on fixtures.

### assertResponseEquals

Asserts that a Response equals to a pre-recorded expected response
(which was recorded as [TestHttpResponse](../../src/SymfonyTestUtils/Fixture/TestHttpResponse.php)).

The expected response should be recorded by [ResponseFixtureTrait](../../src/SymfonyTestUtils/Fixture/ResponseFixtureTrait.php).

# [ResponseFixtureTrait](../../src/SymfonyTestUtils/Fixture/ResponseFixtureTrait.php)

Helper class with methods to read and write expected Http responses.

# [TextFixtureTrait](../../src/SymfonyTestUtils/Fixture/TextFixtureTrait.php)

Helper class with methods to read and write general text files.
