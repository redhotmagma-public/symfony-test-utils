Constraints Content
===================

## [AssertThat](../../src/SymfonyTestUtils/Constraint/AssertThat.php)

#### modelEquals

Asserts that a model (that means, any model-like object) is of same class as an expected one,
and their chosen properties are equal.

Those properties are accessed either directly (using reflection) or by calling their getter method. In the latter,
the method should be called getMyProperty and should not accept any parameters.

When no properties are set, works as TestCase::assertEquals.

Example:
```php
class A
{
    public $property;

    public $ignored;

    public function __construct($property, $ignored)
    {
        $this->property = $property;
        $this->ignored = $ignored;
    }
}

$expected = new A('Foo', 'NotBar');
$actual = new A('Foo', 'Bar');

// Test will pass, since both objects are of same class, and
// the validated "property" value equals in both objects
AssertThat::modelEquals($expected, $actual, ['property']);

// This will fail, since "ignored" is different.
AssertThat::modelEquals($expected, $actual, ['ignored']);

// This will fail, since it will fall back to TestCase::assertEquals
AssertThat::modelEquals($expected, $actual);

// This will fail, since objects are from different classes
AssertThat::modelEquals($expected, new \stdClass());
```
