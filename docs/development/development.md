Development
===========

## Client
Provides a builder for Symfony's client. Other Cliente helpers may be created there.

## Library Structure
### Constraint
Here resides all implementations to PHPUnit\Framework\Constraint\Constraint, which is used natively by PHPunit.
Factory methods using those contraints should be placed in [AssertThat](./src/SymfonyTestUtils/Constraint/AssertThat.php) class.

### Fixture
Collection of traits and classes to help reading and writing fixture files.
Factory methods using those helpers should be placed in [Fixtures](./src/SymfonyTestUtils/Fixture/Fixtures.php) class.
