#!/usr/bin/env bash

PROJECT_HOME='/app'
PATH=${PATH}:${PROJECT_HOME}/vendor/bin

alias cdproject="cd $PROJECT_HOME"
alias ll='ls -laH --color=auto --group-directories-first'
alias phpunit="${PROJECT_HOME}/vendor/bin/simple-phpunit"
