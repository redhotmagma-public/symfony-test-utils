#!/usr/bin/env bash


project=/app

# 0 clear the cache
php ${project}/bin/console cache:clear --env=prod --no-warmup
php ${project}/bin/console cache:warmup --env=prod

# 1 get basic auth credentials
php ${project}/bin/console vault:prepare:basic-auth


# 2. load environment variables
php ${project}/bin/console vault:prepare:environment

# 3. set correct user rights (script is running as root)
chown -R application ${project}/var/