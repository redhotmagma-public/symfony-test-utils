#!/usr/bin/env bash

source ./.env

CONTAINER_ID=$(docker ps -aqf "name=docker_${PROJECT_NAME}")

docker exec -i -t ${CONTAINER_ID} /bin/bash
